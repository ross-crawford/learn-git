import React from "react";
import { MenuItem } from "react-pro-sidebar";
import { Link } from "react-router-dom";

const MenuLink = ({ title, path, icon }) => {
  return (
    <MenuItem icon={icon}>
      {title}
      <Link to={path} />
    </MenuItem>
  );
};

export default MenuLink;
