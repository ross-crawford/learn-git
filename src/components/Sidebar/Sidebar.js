import React from "react";
import {
  ProSidebar,
  Menu,
  SubMenu,
  SidebarHeader,
  SidebarContent,
  SidebarFooter,
} from "react-pro-sidebar";
import MenuLink from "./MenuLink";
import {
  RiHome2Line,
  RiSettings2Line,
  RiCodeSSlashLine,
  RiRocket2Line,
  RiInformationLine,
  RiGift2Line,
  RiNumber1,
  RiNumber2,
  RiNumber3,
  RiNumber4,
  RiNumber5,
  RiNumber6,
  RiNumber7,
  RiNumber8,
} from "react-icons/ri";
import "./Sidebar.scss";

const Sidebar = ({ isCollapsed, isToggled }) => {
  return (
    <ProSidebar breakPoint="lg" collapsed={isCollapsed} toggled={isToggled}>
      <SidebarHeader>
        <div className="sidebar-header">Learn Git with React</div>
      </SidebarHeader>
      <SidebarContent>
        <Menu iconShape="circle">
          {/* Home Link */}
          <MenuLink title="Home" path="/" icon={<RiHome2Line />} />
          {/* Chapter 1 */}
          <SubMenu
            title="Get Up and Running with Git"
            icon={<RiSettings2Line />}
          >
            <MenuLink
              title="What is Git?"
              path="/chapter-one/section-one"
              icon={<RiNumber1 />}
            />
            <MenuLink
              title="Using the Command Line"
              path="/chapter-one/section-two"
              icon={<RiNumber2 />}
            />
            <MenuLink
              title="Installing Git"
              path="/chapter-one/section-three"
              icon={<RiNumber3 />}
            />
            <MenuLink
              title="Configuring Git"
              path="/chapter-one/section-four"
              icon={<RiNumber4 />}
            />
            <MenuLink
              title="Initialise a Git Repository"
              path="/chapter-one/section-five"
              icon={<RiNumber5 />}
            />
            <MenuLink
              title="Pushing to a Remote Repository"
              path="/chapter-one/section-six"
              icon={<RiNumber6 />}
            />
          </SubMenu>
          {/* Chapter 2 */}
          <SubMenu
            title="Basic Commands of Everyday Git"
            icon={<RiCodeSSlashLine />}
          >
            <MenuLink
              title="Git Status"
              path="/chapter-two/section-one"
              icon={<RiNumber1 />}
            />
            <MenuLink
              title="File Stages"
              path="/chapter-two/section-two"
              icon={<RiNumber2 />}
            />
            <MenuLink
              title="Short Status"
              path="/chapter-two/section-three"
              icon={<RiNumber3 />}
            />
            <MenuLink
              title="Git Diff Explained"
              path="/chapter-two/section-four"
              icon={<RiNumber4 />}
            />
            <MenuLink
              title="Commit Changes"
              path="/chapter-two/section-five"
              icon={<RiNumber5 />}
            />
          </SubMenu>
          {/* Chapter 3 */}
          <SubMenu
            title="Extended Commands of Everyday Git"
            icon={<RiRocket2Line />}
          >
            <MenuLink
              title="Push to Remote Origin"
              path="/chapter-three/section-one"
              icon={<RiNumber1 />}
            />
            <MenuLink
              title="Check Commit History"
              path="/chapter-three/section-two"
              icon={<RiNumber2 />}
            />
            <MenuLink
              title="Commit Messages"
              path="/chapter-three/section-three"
              icon={<RiNumber3 />}
            />
            <MenuLink
              title="Remove and Move Files"
              path="/chapter-three/section-four"
              icon={<RiNumber4 />}
            />
            <MenuLink
              title="Introduction to Branches"
              path="/chapter-three/section-five"
              icon={<RiNumber5 />}
            />
            <MenuLink
              title="Merge"
              path="/chapter-three/section-six"
              icon={<RiNumber6 />}
            />
            <MenuLink
              title="Reset"
              path="/chapter-three/section-seven"
              icon={<RiNumber7 />}
            />
            <MenuLink
              title="Pull from Remote Origin"
              path="/chapter-three/section-eight"
              icon={<RiNumber8 />}
            />
          </SubMenu>
          {/* Other pages */}
          <MenuLink
            title="Resources"
            path="/resources"
            icon={<RiInformationLine />}
          />
          <MenuLink
            title="Cheat Sheet"
            path="/cheat-sheet"
            icon={<RiGift2Line />}
          />
        </Menu>
      </SidebarContent>
      <SidebarFooter>
        <div className="sidebar-footer">&copy; Ross Crawford 2021</div>
      </SidebarFooter>
    </ProSidebar>
  );
};

export default Sidebar;
