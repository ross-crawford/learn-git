import React from "react";
import { ScaleLoader } from "react-spinners";
import "./Spinner.scss";

const Spinner = () => {
  return (
    <section className="spinner">
      <ScaleLoader
        color="#FFFFFF"
        height={50}
        width={5}
        radius={2}
        margin={5}
      />
    </section>
  );
};

export default Spinner;
