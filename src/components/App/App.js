import { useState } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Header from "../Header/Header";
import Sidebar from "../Sidebar/Sidebar";
import MenuBtn from "../MenuBtn/MenuBtn";
import Home from "../../pages/Home/Home";
import ChapterOneSectionOne from "../../pages/ChapterOne/SectionOne";
import ChapterOneSectionTwo from "../../pages/ChapterOne/SectionTwo";
import ChapterOneSectionThree from "../../pages/ChapterOne/SectionThree";
import ChapterOneSectionFour from "../../pages/ChapterOne/SectionFour";
import ChapterOneSectionFive from "../../pages/ChapterOne/SectionFive";
import ChapterOneSectionSix from "../../pages/ChapterOne/SectionSix";
import ChapterTwoSectionOne from "../../pages/ChapterTwo/SectionOne";
import ChapterTwoSectionTwo from "../../pages/ChapterTwo/SectionTwo";
import ChapterTwoSectionThree from "../../pages/ChapterTwo/SectionThree";
import ChapterTwoSectionFour from "../../pages/ChapterTwo/SectionFour";
import ChapterTwoSectionFive from "../../pages/ChapterTwo/SectionFive";
import ChapterThreeSectionOne from "../../pages/ChapterThree/SectionOne";
import ChapterThreeSectionTwo from "../../pages/ChapterThree/SectionTwo";
import ChapterThreeSectionThree from "../../pages/ChapterThree/SectionThree";
import ChapterThreeSectionFour from "../../pages/ChapterThree/SectionFour";
import ChapterThreeSectionFive from "../../pages/ChapterThree/SectionFive";
import ChapterThreeSectionSix from "../../pages/ChapterThree/SectionSix";
import ChapterThreeSectionSeven from "../../pages/ChapterThree/SectionSeven";
import ChapterThreeSectionEight from "../../pages/ChapterThree/SectionEight";
import Resources from "../../pages/Misc/Resources";
import CheatSheet from "../../pages/Misc/CheatSheet";
import "./App.scss";

const App = () => {
  const [isCollapsed, setIsCollapsed] = useState(false);
  const [isToggled, setIsToggled] = useState(false);

  const handleCollapseChange = () => setIsCollapsed(!isCollapsed);
  const handleToggleChange = () => setIsToggled(!isToggled);
  return (
    <Router>
      <div className="app">
        <Sidebar
          isCollapsed={isCollapsed}
          isToggled={isToggled}
          handleCollapse={handleCollapseChange}
        />
        <main style={{ position: "relative" }}>
          <MenuBtn handleToggle={handleToggleChange} isToggled={isToggled} />
          <Header title="Learn Git with React" />
          <hr />
          <Switch>
            <Route exact path="/" component={Home} />
            <Route
              path="/chapter-one/section-one"
              component={ChapterOneSectionOne}
            />
            <Route
              path="/chapter-one/section-two"
              component={ChapterOneSectionTwo}
            />
            <Route
              path="/chapter-one/section-three"
              component={ChapterOneSectionThree}
            />
            <Route
              path="/chapter-one/section-four"
              component={ChapterOneSectionFour}
            />
            <Route
              path="/chapter-one/section-five"
              component={ChapterOneSectionFive}
            />
            <Route
              path="/chapter-one/section-six"
              component={ChapterOneSectionSix}
            />
            <Route
              path="/chapter-two/section-one"
              component={ChapterTwoSectionOne}
            />
            <Route
              path="/chapter-two/section-two"
              component={ChapterTwoSectionTwo}
            />
            <Route
              path="/chapter-two/section-three"
              component={ChapterTwoSectionThree}
            />
            <Route
              path="/chapter-two/section-four"
              component={ChapterTwoSectionFour}
            />
            <Route
              path="/chapter-two/section-five"
              component={ChapterTwoSectionFive}
            />
            <Route
              path="/chapter-three/section-one"
              component={ChapterThreeSectionOne}
            />
            <Route
              path="/chapter-three/section-two"
              component={ChapterThreeSectionTwo}
            />
            <Route
              path="/chapter-three/section-three"
              component={ChapterThreeSectionThree}
            />
            <Route
              path="/chapter-three/section-four"
              component={ChapterThreeSectionFour}
            />
            <Route
              path="/chapter-three/section-five"
              component={ChapterThreeSectionFive}
            />
            <Route
              path="/chapter-three/section-six"
              component={ChapterThreeSectionSix}
            />
            <Route
              path="/chapter-three/section-seven"
              component={ChapterThreeSectionSeven}
            />
            <Route
              path="/chapter-three/section-eight"
              component={ChapterThreeSectionEight}
            />
            <Route path="/resources" component={Resources} />
            <Route path="/cheat-sheet" component={CheatSheet} />
          </Switch>
        </main>
      </div>
    </Router>
  );
};

export default App;
