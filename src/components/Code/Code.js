import React from 'react'

const Code = ({children, block}) => {

    if (block) {
        return (
            <pre>
                <code>{children}</code> 
            </pre>
        )
    } else {
        return (
            <code>{children}</code>
        )
    }
}

export default Code
