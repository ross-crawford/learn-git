import React from "react";
import Hamburger from "hamburger-react";
import "./MenuBtn.scss";

const MenuBtn = ({ handleToggle, isToggled }) => {
  return (
    <div className="menu-btn">
      <Hamburger
        toggled={isToggled}
        toggle={handleToggle}
        onClick={handleToggle}
        size={20}
      />
    </div>
  );
};

export default MenuBtn;
