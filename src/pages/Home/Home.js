import React from "react";
import Section from "../../components/Section/Section";
import { Link } from "react-router-dom";

const Home = () => {
  return (
    <Section title="Welcome">
      <p>
        Hello, I'd like to welcome you to my Learn Git with React tutorial
        website. This site serves as <strong>two</strong> main purposes:
      </p>
      <ol>
        <li>A place for me to document my findings while learning Git</li>
        <li>A tool for me to use when practicing Git</li>
      </ol>
      <p>
        It's probably also worth mentioning that this website is fully
        functional and a complete list of the reasources used to learn Git and
        create this site can be found <Link to="/resources">here</Link>.
      </p>
      <Link to="/chapter-one/section-one">
        <button>Start Learning</button>
      </Link>
    </Section>
  );
};

export default Home;
