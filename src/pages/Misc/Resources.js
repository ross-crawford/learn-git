import React from "react";
import Section from "../../components/Section/Section";
import { gitData, devData } from "./resourceData";
import "./Resources.scss";

const Resources = () => {
  return (
    <Section title="Resources">
      <p>
        Below is a list of all resources used in the creation of this website.
        This includes the resources utilised for learning Git and Github, in
        addition to the tools used in the development of the React website
        itself.
      </p>
      <div className="flex">
        <div>
          <h4>Resources for learning Git</h4>
          <ul>
            {gitData.map((item, index) => (
              <li key={index}>
                <a href={item.url} rel="noopener noreferrer" target="_blank">
                  {item.title}
                </a>
              </li>
            ))}
          </ul>
        </div>
        <div>
          <h4>Resources for building the website</h4>
          <ul>
            {devData.map((item, index) => (
              <li key={index}>
                <a href={item.url} rel="noopener noreferrer" target="_blank">
                  {item.title}
                </a>
              </li>
            ))}
          </ul>
        </div>
      </div>
    </Section>
  );
};

export default Resources;
