import React from "react";
import Section from "../../components/Section/Section";
import Code from "../../components/Code/Code";
import { cheatSheetData } from "./resourceData";
import "./Resources.scss";

const CheatSheet = () => {
  return (
    <Section title="Cheat Sheet">
      <p>A cheat sheet for some of the most commonly used git commands.</p>
      <div className="grid">
        {cheatSheetData.map(({ id, title, description, command }) => (
          <div key={id}>
            <h4>{title}</h4>
            <p>{description}</p>
            <p>
              <Code block>{command}</Code>
            </p>
          </div>
        ))}
      </div>
    </Section>
  );
};

export default CheatSheet;
