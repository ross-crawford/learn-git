export const gitData = [
  {
    title: "Pluralsight Course by Aaron Stewart",
    url: "https://app.pluralsight.com/library/courses/getting-started-git",
  },
  {
    title: "Git Book",
    url: "https://git-scm.com/book/en/v2",
  },
  {
    title: "Git Downloads",
    url: "https://git-scm.com/downloads",
  },
  {
    title: "GitHub",
    url: "https://github.com/",
  },
  {
    title: "GitLab",
    url: "https://gitlab.com/",
  },
  {
    title: "Bitbucket",
    url: "https://bitbucket.org/",
  },
];
export const devData = [
  {
    title: "React",
    url: "https://reactjs.org/",
  },
  {
    title: "React Router",
    url: "https://reactrouter.com/",
  },
  {
    title: "React Icons",
    url: "https://react-icons.github.io/react-icons",
  },
  {
    title: "Hamburger React",
    url: "https://hamburger-react.netlify.app/",
  },
  {
    title: "React Pro Sidebar",
    url: "https://github.com/azouaoui-med/react-pro-sidebar",
  },
  {
    title: "React Spinners",
    url: "https://www.davidhu.io/react-spinners/",
  },
  {
    title: "Node Sass",
    url: "https://github.com/sass/node-sass",
  },
  {
    title: "Emotion",
    url: "https://github.com/emotion-js/emotion/tree/main/packages/react",
  },
];

export const cheatSheetData = [
  {
    id: 1,
    title: "Stage a single file",
    description:
      "Add a single file to the staging area, specified by file name",
    command: "git add <filename>",
  },
  {
    id: 2,
    title: "Stage all files",
    description: "Add all new or modified files to the staging area",
    command: "git add .",
  },
  {
    id: 3,
    title: "Commit changes with message",
    description: "Commit all staged files to a new snapshot",
    command: "git commit -m '<message>'",
  },
  {
    id: 4,
    title: "Push commit to remote",
    description: "Push all new local commits to the remote repository",
    command: "git push origin main",
  },
  {
    id: 5,
    title: "View status of local repository",
    description:
      "Display a detailed status of local repository, including any staged/unstaged files",
    command: "git status",
  },
];
