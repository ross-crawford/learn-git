import React from "react";
import Section from "../../components/Section/Section";
import Code from "../../components/Code/Code";

const SectionSeven = () => {
  return (
    <Section title="Reset">
      <p>
        <strong>
          Warning! The reset command can be destructive so please use it with
          caution.
        </strong>
        With that being said, the first thing to be aware of is that there are
        three options for git reset.
      </p>
      <ul>
        <li>
          <Code>git reset --soft</Code> - this will reset the changes back to
          the staging area (tracked)
        </li>
        <li>
          <Code>git reset --mixed</Code> - this will reset the changes back to
          the working directory (untracked)
        </li>
        <li>
          <Code>git reset --hard</Code> - this will competely remove the changes
        </li>
      </ul>
      <p>
        <em>
          A useful tip is that you can use the reset command to undo an unwanted
          commit, e.g. if you committed before a change was complete. Simply run
          the command <Code>git reset --soft HEAD~1</Code>.
        </em>
      </p>
    </Section>
  );
};

export default SectionSeven;
