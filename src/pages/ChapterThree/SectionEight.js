import React from "react";
import Section from "../../components/Section/Section";
import Code from "../../components/Code/Code";

const SectionEight = () => {
  return (
    <Section title="Pull from Remote Origin">
      <p>
        Firstly, if there is no local version of a repository you can simply
        navigate to the remote repository on whichever hosting platform it is on
        and click the 'Clone' button and copying the HTTPS link (it is possible
        to use the SSH link but it requires additional setup). Once you have the
        link copied, open up the command line and cd into whichever directory on
        your computer you want to clone the repository to, e.g.{" "}
        <Code>cd ~/projects</Code> and run the command{" "}
        <Code>git clone &lt;repository url&gt;</Code> and this will create a
        copy of the repository in your chosen directory.
      </p>
      <p>
        Now let's say you clone a repository and then someone else pushes some
        changes to the remote repository. Your local version is now 1 commit
        behind the remote - you can confirm this using the{" "}
        <Code>git status</Code> command. In order to ensure you have the most up
        to date version, you'll need to pull the recent changes down to your
        local repository. To do this, run the{" "}
        <Code>git pull origin master</Code> command. This will pull down any
        recent changes and include them in your local repository.
      </p>
      <p>
        If you're working on a branch, the same problem will occur when you go
        to push to the remote if there have been other changes made since you
        initially started your branch. The same command as above can be used to
        pull from a different branch on the remote (in the example above it will
        be from the master branch).
      </p>
    </Section>
  );
};

export default SectionEight;
