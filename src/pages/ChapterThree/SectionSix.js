import React from "react";
import Section from "../../components/Section/Section";
import Code from "../../components/Code/Code";

const SectionSix = () => {
  return (
    <Section title="Merge">
      <p>
        Merging is the act of combining the commits from two branches into one -
        usually into the master branch.{" "}
        <strong>
          It is worth noting at this point that merging is typically done on the
          remote using Pull Requests, which will be covered later.
        </strong>
      </p>
      <p>
        Merging can be done locally, however, and the same rules apply as doing
        so remotely. Another thing to be aware of when merging is Merge
        Conflicts, which is a more advanced topic to be covered later. To set up
        for a merge, first create a new branch and swatch to it. Make some
        changes to this branch and commit them. Now that the changes are
        committed, you can switch back to the master branch and are ready to
        merge.
      </p>
      <p>
        Run the command <Code>git merge &lt;branch name&gt;</Code> as you are
        merging into your current branch from the branch specified. This command
        will open <abbr title="Vi Improved">VIM</abbr> in your command line. If
        you've never used VIM before, this part can get a bit tricky. All you
        need to know is you can press <kbd>i</kbd> to enter 'Insert' mode, where
        you can then navigate to the commit message and change it if you wish.
        Press <kbd>Esc</kbd> to exit 'Insert' mode and then hit <kbd>!</kbd>
        &nbsp;
        <kbd>w</kbd>&nbsp;
        <kbd>q</kbd> to complete the merge. For reference, the <kbd>w</kbd> will
        write the changes and the <kbd>q</kbd> will quite VIM.
      </p>
      <p>
        A response will be displayed once the merge is complete, confirming this
        and detailing a high level overview of the changes made. If you run{" "}
        <Code>git status</Code> now, you'll see that the master branch is now at
        least 1 commit ahead of the 'origin/master' - you can simply push to the
        remote repository now to bring it up to date with the local version.
      </p>
    </Section>
  );
};

export default SectionSix;
