import React from "react";
import Section from "../../components/Section/Section";
import Code from "../../components/Code/Code";

const SectionFour = () => {
  return (
    <Section title="Remove and Move Files">
      <p>
        Removing a file from a project can feel a bit awkward given the previous
        sections have all focused on adding new files or changing existing
        files. It's actually very easy to do, simply run the command{" "}
        <Code>git rm &lt;filename&gt;</Code>.
      </p>
      <p>
        Alternatively, just delete the file the old fashioned way... both ways
        work fine.
      </p>
      <p>
        If you want to keep a file, but simply untrack it, use the{" "}
        <Code>git rm --cached &lt;filename&gt;</Code> command instead. You can
        view the results of this by simply running it after a{" "}
        <Code>git add .</Code> and you'll see the file you untracked under the
        'Untracked files' section.
      </p>
      <p>
        There is also a command for renaming files (again, if you don't want to
        do that the old fashioned way) which is{" "}
        <Code>git mv &lt;current filename&gt; &lt;desired filename&gt;</Code>.
        Please note, the file you are attempting to change must be tracked.
      </p>
    </Section>
  );
};

export default SectionFour;
