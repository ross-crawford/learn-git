import React from "react";
import Section from "../../components/Section/Section";
import Code from "../../components/Code/Code";

const SectionOne = () => {
  return (
    <Section title="Push to Remote Origin">
      <p>
        Pushing to a remote version of a repository is when you push your local
        commits to the remote version of the project on the hosting site.
        Remember, when you commit a change, this commit only resides on your
        local version and is only available to you. In order for others to gain
        access to your changes, they must be pushed to the remote.
      </p>
      <p>
        It is worth noting that you don't need to push every time you make a
        commit, only when you need your commits to be available to others or be
        accessible on your remote repository.
      </p>
      <p>
        There are a variety of options for the push command but the basic syntax
        of it is <Code>git push origin master</Code>. This command simply tells
        git to push the local current branch to the remote version of the master
        branch. If the master branch did not exist remotely, it would be
        created.
      </p>
      <p>
        The command will return a response confirming the push has been
        successful, or any errors if they occur. The response will show the
        remote repository url being pushed to and the SHA1 hash of the new
        commit on the remote.
      </p>
    </Section>
  );
};

export default SectionOne;
