import React from "react";
import Section from "../../components/Section/Section";
import Code from "../../components/Code/Code";

const SectionFive = () => {
  return (
    <Section title="Introduction to Branches">
      <p>
        Branches provide a more organised way to make changes to a repository
        without cluttering up the master branch with lots of commits. Typically
        you will create a new branch any time you are changing something on a
        project (particularly in a professional environment). To create a new
        branch, run the command <Code>git checkout -b &lt;branchname&gt;</Code>{" "}
        - this will create the new branch and switch to it. Alternatively, you
        can run <Code>git branch &lt;branchname&gt;</Code> to create the branch
        but keep the current branch you are on checked out. You can then run{" "}
        <Code>checkout &lt;branchname&gt;</Code> to switch to it.
      </p>
      <p>
        Please note, any files which were modified/added prior to
        creating/swapping to the new branch will be carried over to that branch
        if they have not been committed. This can be useful if you start working
        on a change but forget to swap branches, simply create the branch and
        checkout to move your new changes across and continue working.{" "}
        <strong>
          Once the changes have been committed to a specific branch, they will
          no longer e available if you swap to another branch.
        </strong>
      </p>
      <p>Some other useful commands relating to branches are:</p>
      <ul>
        <li>
          <Code>git branch</Code> - lists all local branches
        </li>
        <li>
          <Code>git branch -r</Code> - lists all remote branches
        </li>
        <li>
          <Code>git branch -a</Code> - lists all local and remote branches
        </li>
        <li>
          <Code>git stash</Code> - stashes uncommitted changes away to a WIP
          branch
        </li>
        <li>
          <Code>git stash pop</Code> - unstashes the last entry to the WIP
          branch
        </li>
        <li>
          <Code>git stash list</Code> - lists all the current stashes
        </li>
        <li>
          <Code>git stash show</Code> - lists the modified files in the current
          stash
        </li>
      </ul>
      <p>
        A great website for testing out and visualising how branches (among
        other things) work is called{" "}
        <a
          href="https://git-school.github.io/visualizing-git/"
          trget="_blank"
          rel="noopener noreferrer"
        >
          Visualising Git
        </a>
        . You can test different git commands and create branches, practice
        merging them, etc. and the visualisation really helps cement these
        concepts.
      </p>
    </Section>
  );
};

export default SectionFive;
