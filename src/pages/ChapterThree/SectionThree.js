import React from "react";
import Section from "../../components/Section/Section";
import Code from "../../components/Code/Code";

const SectionThree = () => {
  return (
    <Section title="Commit Messages">
      <p>
        As seen in the previous section, commit messages are visible to
        everyone, and should accurately represent the changes being made within
        the commit. There are many different techniques to commit messages, and
        best practices can vary from company to company.
      </p>
      <p>
        Guidelines for commit messages may be specified by your organisation,
        and for the sake of consistency, those should always be followed. A few
        general rules for commit messages are as follows:
      </p>
      <ul>
        <li>
          Specify the type of change, .e.g. fix, feat, chore, etc. - this will
          help others understand what category of changes were being made within
          this commit.
        </li>
        <li>
          Specify the actual changes being made in a concise manner, e.g. fixed
          bug preventing user from clicking sign up button
        </li>
        <li>
          If your organisation uses issue tracking software like Jira, you
          should include the ticket number that the commit changes fix in the
          message
        </li>
        <li>Avoid any vague or unnecessary details, e.g. 'wip' or 'pls fix'</li>
      </ul>
      <p>
        Chris Beam wrote a great blog post on this in 2014, although it still
        holds true by modern standards. You can read it yourself{" "}
        <a
          href="https://chris.beams.io/posts/git-commit/"
          target="_blank"
          rel="noopener noreferrer"
        >
          here
        </a>
        .
      </p>
      <h4>Git Commit Command Line Tools</h4>
      <p>
        Many companies utilise command line tools for commit messages nowadays,
        as it ensures a level of consistency across everyone contributing to a
        project and helps minimise mistakes. One of the most popular options for
        this when using Jira is a tool called{" "}
        <a
          href="https://github.com/commitizen/cz-cli"
          target="_blank"
          rel="noopener noreferrer"
        >
          Commitizen
        </a>{" "}
        which you can install as a dev dependency in a project and run with the{" "}
        <Code>git-cz</Code> (or any custom script you create). This will display
        a number of prompts to provide information, such as ticket numbers,
        short descriptions, long descriptions, type of change, etc. and can even
        be customised to suit the needs of your project or organisation.
      </p>
    </Section>
  );
};

export default SectionThree;
