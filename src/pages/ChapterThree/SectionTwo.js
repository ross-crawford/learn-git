import React from "react";
import Section from "../../components/Section/Section";
import Code from "../../components/Code/Code";

const SectionTwo = () => {
  return (
    <Section title="Check Commit History">
      <p>
        Providing a complete commit history of a repository is another useful
        feature of git. This allows you to view every commit ever made to a
        projcet in reverse order to which they were made (meaning you'll always
        see the most recent commit first, also known as the HEAD).
      </p>
      <p>
        To view the commit history simply run the command <Code>git log</Code>{" "}
        or <Code>git lg</Code> for the condensed version (similar to the Short
        Status). This command will return a list of all commits, including:
      </p>
      <ul>
        <li>The full commit SHA1 hash</li>
        <li>The commit author and their email</li>
        <li>The Date timestamp of the commit</li>
        <li>The commit message</li>
      </ul>
      <p>
        Please note, using the condensed version <Code>git lg</Code> will not
        return the full hash, the email, or the timestamp (although it will
        shorten the date to an 'x minutes/hours/days ago' message.)
      </p>
      <p>
        You can also limit the number of commits in the log by simply running
        the previous command and appending the number of commits you wish to
        see, e.g. <Code>git log -3</Code> will return the 3 most recent commits.
        This also works for the shortened version too.
      </p>
      <p>
        Another useful variation of this command is <Code>git log --stat</Code>{" "}
        which will return the same information as the normal version of the
        command, bbut will also include a full list of the files changed along
        with the number of insertions/deletions. This can be useful for tracking
        down bugs, as it allows you to find the commits where a specific file
        was changed (although there are some more advanced commands tailored
        specifically for this purpose - those may be covered in a later
        section).
      </p>
      <p>
        <strong>And if that's not enough information for you</strong>, you can
        obtain an even more detailed response using the{" "}
        <Code>git log --patch</Code> command. This response will include
        everything from <Code>git log --stat</Code> as well as a breakdown of
        the specific changes within each file. This section of the response
        looks very similar to the <Code>git diff</Code> command. Please note,
        the responses from this command can be very long so you can also combine
        the patch flag with the number of responses technique above, e.g.{" "}
        <Code>git log -2 --patch</Code>.
      </p>
      <p>
        Similar to <Code>git diff</Code> there are a lot of variations of this
        command, and it would be worthwhile to read more on these using the{" "}
        <Code>git help log</Code> command.
      </p>
    </Section>
  );
};

export default SectionTwo;
