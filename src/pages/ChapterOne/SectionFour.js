import React from "react";
import Section from "../../components/Section/Section";
import Code from "../../components/Code/Code";

const SectionFour = () => {
  return (
    <Section title="Configuring Git">
      <p>
        Some configuration is required to set up a username and email within
        git. The following steps demonstrate how to do this
      </p>
      <ul>
        <li>
          Set up username with the command{" "}
          <Code>git config --global user.name "Your Name"</Code>
        </li>
        <li>
          Set up email with the command{" "}
          <Code>git config --global user.email "your@email.com"</Code>
        </li>
      </ul>
      <p>
        You can always check the full list of config settings by running the
        command <Code>git config --list</Code>
      </p>
      <p>
        Another two useful command to remember are the <Code>git help</Code>{" "}
        (returns a list of common git commands) and <Code>man git</Code>{" "}
        (returns the full git manual - very long).
      </p>
    </Section>
  );
};

export default SectionFour;
