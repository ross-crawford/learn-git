import React from "react";
import Section from "../../components/Section/Section";
import Code from "../../components/Code/Code";

const SectionThree = () => {
  return (
    <Section title="Installing Git">
      <p>
        Before you can use git in the command line, you'll first need to install
        it on your machine. Installing git is a simply process, just follow the
        steps below to get set up
      </p>
      <ol>
        <li>
          Visit <a href="https://git-scm.com/downloads">the git website</a>
        </li>
        <li>Download the appropriate version for your OS</li>
        <li>One the download is complete, run the installer</li>
      </ol>
      <p>
        Once the installation is complete, you can verify this by opening your
        terminal and entering the command <Code>git --version</Code> which
        should print out the current version of git installed.
      </p>
    </Section>
  );
};

export default SectionThree;
