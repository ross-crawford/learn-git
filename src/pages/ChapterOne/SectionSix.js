import React from "react";
import Section from "../../components/Section/Section";
import screenshot from "../../assets/images/screenshot1.png";

const SectionSix = () => {
  return (
    <Section title="Pushing to a Remote Repository">
      <p>
        After you have created a repository, you will generally be shown a list
        of commands to push your local repository to the remote. Below are the
        commands provided by GitHub.
      </p>
      <img
        src={screenshot}
        alt="GitHub commands on new repository"
        className="git-screenshot"
      />
      <p>
        Because this application is built with Create React App, there is
        already some initialisation done, such as creating a README.md file and
        .git folder. Therefore, I will be using the second set of commands to
        push this project to the remote repository.
      </p>
    </Section>
  );
};

export default SectionSix;
