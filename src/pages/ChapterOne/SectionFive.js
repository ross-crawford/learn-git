import React from "react";
import Section from "../../components/Section/Section";
import Code from "../../components/Code/Code";

const SectionFive = () => {
  return (
    <Section title="Initialise a Git Repository">
      <p>
        The first step to initialise a git repository is to <Code>cd</Code> into
        the directory where the project lives, for example{" "}
        <Code>cd projects/learn_git</Code>
      </p>
      <p>
        Now that you're in the correct directory, you can run the{" "}
        <Code>git init</Code> command to initialise the git project. This will
        create a .git subdirectory, which will add the git configuration to mark
        the project folder and it's contents as being version controlled. The
        .git folder is hidden by default, but you can <Code>cd</Code> cd into it
        and run the <Code>ls</Code> command to view its contents.
      </p>
      <h4>Choose a Code Hosting Provider</h4>
      <p>
        Now that the local project has been initialised, it's time to choose a
        provider to host the remote version. There are a few to choose from, but
        some of the most popular ones are:
      </p>
      <ul>
        <li>
          <a href="https://github.com/">GitHub</a>
        </li>
        <li>
          <a href="https://gitlab.com/">GitLab</a>
        </li>
        <li>
          <a href="https://bitbucket.org/">Bitbucket</a>
        </li>
      </ul>
      <p>
        You will need to create an account for the provider of your choice. Once
        you have an account, you can then create a new repository - this process
        is very similar across most providers, as is the the way in which you
        push your local repository to the remote repository.
      </p>
    </Section>
  );
};

export default SectionFive;
