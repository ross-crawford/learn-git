import React from "react";
import Section from "../../components/Section/Section";
import Code from "../../components/Code/Code";

const SectionTwo = () => {
  return (
    <Section title="Using the Command Line">
      <p>
        The command line is used to run commands. Specific commands are entered
        and executed (within the current directory). There are a number of
        command line tools which can be used, some of which are OS specific.
      </p>
      <ul>
        <li>Command Prompt or PowerShell for Windows</li>
        <li>Terminal or iTerm for Mac</li>
        <li>Git Bash for all OS</li>
        <li>and more...</li>
      </ul>
      <p>
        A simple command you can use in the terminal to test it out is{" "}
        <Code>pwd</Code> which prints the current working directory, e.g.{" "}
        <Code>/Users/Name/Projects</Code>
      </p>
      <p>
        You can also change directory using the <Code>cd</Code> command.
        Examples include <Code>cd ..</Code> to move up one directory level, or{" "}
        <Code>cd subfolder</Code> to move down to a specific child directory.
      </p>
      <p>
        The <Code>ls</Code> command on Mac (or <Code>dir</Code> for Windows)
        will list out all files in the current directory.
      </p>
      <p>
        The <Code>mkdir</Code> command will create a new empty folder in the
        current directory.
      </p>
    </Section>
  );
};

export default SectionTwo;
