import React from "react";
import Section from "../../components/Section/Section";

const SectionOne = () => {
  return (
    <Section title="What is Git?">
      <p>Git is a Version Control System, also known as VCS.</p>
      <ul>
        <li>Software designed to record changes made to files over time.</li>
        <li>
          Ability to revert back to a previous file version or project version
        </li>
        <li>Compare changes made to files from one version to another</li>
        <li>Version control any plain text file, not just source code</li>
      </ul>
      <p>Version Control is like a time travel machine for your project</p>
      <h4>The Three Stages of a File</h4>
      <ul>
        <li>
          <strong>Committed</strong>: The data in the file is safely stored
        </li>
        <li>
          <strong>Modified</strong>: The data in the file has changed from what
          is stored
        </li>
        <li>
          <strong>Staged</strong>: The changes are staged, ready to be added to
          the next commit
        </li>
      </ul>
      <h4>The Three Main Parts of Git</h4>
      <ul>
        <li>
          <strong>Working Directory</strong>: The local version of the project
          being worked on, and will remain here unless committed or stashed
        </li>
        <li>
          <strong>Staging Area (Index)</strong>: A single version (or checkout)
          of the project that is between the Working directory and the .git
          directory. Commits will only commit what is in the staging area
        </li>
        <li>
          <strong>.git Directory (Repository)</strong>: This is the origin of
          the project data and what is pulled down from the origin
        </li>
      </ul>
    </Section>
  );
};

export default SectionOne;
