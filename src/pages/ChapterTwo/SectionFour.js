import React from "react";
import Section from "../../components/Section/Section";
import Code from "../../components/Code/Code";

const SectionFour = () => {
  return (
    <Section title="Git Diff Explained">
      <p>
        The <Code>git diff</Code> command is similar to <Code>git status</Code>{" "}
        in the sense that it shows which files have been changed, but it
        provides a more detailed response on what information has actually
        changed within the file. <Code>git diff</Code> answers two main
        questions:
      </p>
      <ol>
        <li>What changes have I staged that are ready to be committed?</li>
        <li>What changes have I made but not yet staged?</li>
      </ol>
      <p>
        To find the answer to question 1, we can run the{" "}
        <Code>git diff --staged</Code> command. This command compares our staged
        changes to our last commit snapshot.
      </p>
      <p>
        For question 2, we can run the <Code>git diff</Code> command on its own
        - this will compare all unstaged files.
      </p>
      <p>
        It is worth mentioning here that the <Code>git diff</Code> command is
        very complex and has a large number of options for its use. Simply run{" "}
        <Code>git help diff</Code> to view more information about how to use
        this command. You'll see that it is possible to modify the output of the
        command, compare specific branches, compare arbitrary commits, and much
        more.
      </p>
    </Section>
  );
};

export default SectionFour;
