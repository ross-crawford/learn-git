import React from "react";
import Section from "../../components/Section/Section";
import Code from "../../components/Code/Code";

const SectionThree = () => {
  return (
    <Section title="Short Status">
      <p>
        While the <Code>git status</Code> command can be very useful for keep
        tracking of the status of files in a project, it can be quite verbose
        and sometimes make identifying the important information quite
        difficult.
      </p>
      <p>
        Git provides a shortened version of this by using either the{" "}
        <Code>git status -s</Code> or <Code>git status --short</Code> command.
        This will simply output a list of the files and their status will be
        shown by either a <span style={{ color: "red" }}>red</span> or{" "}
        <span style={{ color: "green" }}>green</span> letter (or ? symbol)
        before it.
      </p>
      <p>
        Green means the file is being tracked, and red means the file is
        untracked. The letter will either be an 'A' for added for 'M' for
        modified. For new untracked files, you will see a red '??' instead of a
        letter. Once you become familiar with this notation, the short version
        of <Code>git status</Code> can be very useful in providing the relevant
        status information at a quick glance.
      </p>
      <p>
        You may see that sometimes there will be two letters shown. These
        correspond to the 'Staged' and 'Modified' states. The second letter will
        only be visible if there have been changes made to a staged file since
        it was previously staged. The exception to this is for new untracked
        files which will always show two '?' symbols.
      </p>
    </Section>
  );
};

export default SectionThree;
