import React from "react";
import Section from "../../components/Section/Section";
import Code from "../../components/Code/Code";
import screenshot from "../../assets/images/screenshot2.png";

const SectionOne = () => {
  return (
    <Section title="Git Status">
      <p>
        The <Code>git status</Code> command is used to display the current
        working tree status. It provides details such as which branch you are
        currently working on, whether the branch is up to date with the origin
        version of the branch.
      </p>
      <p>
        The command also tells you which files are staged and which files are
        untracked (if any). See the screenshot below for a better idea of the
        information returned.
      </p>
      <img
        src={screenshot}
        alt="Git status results"
        className="git-screenshot"
      />
    </Section>
  );
};

export default SectionOne;
