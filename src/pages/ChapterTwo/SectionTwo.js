import React from "react";
import Section from "../../components/Section/Section";
import Code from "../../components/Code/Code";

const SectionTwo = () => {
  return (
    <Section title="File Stages">
      <p>Each file within a project can be in one of two states:</p>
      <ul>
        <li>Tracked</li>
        <li>Untracked</li>
      </ul>
      <p>
        Tracked files are files which were in the previous git snapshot. These
        files can be in one of three states:
      </p>
      <ul>
        <li>Committed - unmodified files from the last commit snapshot</li>
        <li>Modified - changes made to file since last commit snapshot</li>
        <li>Stged - changes marked to be added to the next commit snapshot</li>
      </ul>
      <h4>Track a New File</h4>
      <p>
        When a new file is added to a project, it will not have been present in
        the previous commit snapshot and will therefore be an untracked file.
        These files can be tracked by running the{" "}
        <Code>git add &lt;filename&gt;</Code> command or simply{" "}
        <Code>git add .</Code> to stage all untracked files.
      </p>
      <p>
        If you then run <Code>git status</Code> you will see the new file(s)
        under the 'Changes to be committed' section. You can also run the
        command <Code>git restore --staged &lt;filename&gt;</Code> to unstage a
        file. The <Code>git status</Code> command provides a few other useful
        commands for handling the staging/unstaging of files and is worth
        checking out.
      </p>
      <h4>Track a Modified File</h4>
      <p>
        Tracking a modified file is the same as for new files, and the same{" "}
        <Code>git add</Code> commands can be used to stage the file with the new
        modifications included.
      </p>
    </Section>
  );
};

export default SectionTwo;
