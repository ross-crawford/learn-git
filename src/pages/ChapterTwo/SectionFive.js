import React from "react";
import Section from "../../components/Section/Section";
import Code from "../../components/Code/Code";

const SectionFive = () => {
  return (
    <Section title="Commit Changes">
      <p>
        Committing changes is when you commit the staged files and changes to
        the new commit snapshot. To do this, simply run the command{" "}
        <Code>git commit -m &lt;your commit message&gt;</Code> and your changes
        will be committed.
      </p>
      <p>
        It is important to note, this command will only commit the files/changes
        which are staged, so to ensure you commit everything run the{" "}
        <Code>git add .</Code> command first. Alternatively, you can skill the{" "}
        <Code>git add .</Code> command and run{" "}
        <Code>git commit -am &lt;your commit message&gt;</Code> which will
        automatically stage all changes before committing.
      </p>
      <p>
        Git will respond with some useful information as an output from the
        command above. This response will tell you the branch being committed
        to, along with the new SHA1 hash for that specific commit and your
        commit message. Additionally, the response will tell you how many files
        were changed and the number of insertions and deletions across those
        files.
      </p>
    </Section>
  );
};

export default SectionFive;
