# Learning Git with React

A React project to implement teachings from Pluralsight course [Getting Started with Git](https://app.pluralsight.com/library/courses/getting-started-git/table-of-contents)

## Chapters

1. Get Up and Running with Git
   1. What is Git?
   2. Using the Command Line
   3. Installing Git
   4. Configuring Git
   5. Initialise a Git Repository
   6. Pushing to a Remote Repository
2. Basic Commands of Everyday Git
   1. Git Status
   2. File Stages
   3. Short Status
   4. Git Diff Explained
   5. Commit Changes
3. Extended Commands of Everyday Git
   1. Push to Remote Origin
   2. Check Commit History
   3. Commit Messages
   4. Remove and Move files
   5. Introduction to Branches
   6. Merge
   7. Reset
   8. Pull from Remote Origin
4. Resources
5. Cheat Sheet
